# Ptrace Remote

A modification of the Linux 4.9.13 kernel adding `ptrace` options for remote
memory and file management.
